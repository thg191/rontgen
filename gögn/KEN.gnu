set encoding utf8
set term post eps enhanced color solid "Helvetica" 20
set ylabel "{/Symbol g}/s [s^{-1}]"
set xlabel "2{/Symbol q} [°]"
set mxtics
#set yrange [0:30]
#set xrange [0:30]
set logscale y
set output 'KEN004_vixl_notitle.eps'
plot 'KEN004_vixl.xy' u 1:2 notitle w l
#set yrange [0:30]
#set xrange [0:30]
set logscale y
set output 'KEN002_vixl_notitle.eps'
plot 'KEN002_vixl.xy' u 1:2 notitle w l
#set yrange [0:30]
#set xrange [0:30]
#unset logscale y
set output 'KEN004_speglun.eps'
plot 'KEN004_speglun.xy' u 1:2 notitle w l
#set yrange [0:30]
#set xrange [0:30]
#set logscale y
set output 'KEN002_speglun_good.eps'
plot 'KEN004_speglun.xy' u 1:2 notitle w l
#set yrange [0:30]
#set xrange [0:30]
#set logscale y
set output 'Speglanir.eps'
plot 'KEN004_speglun.xy' u 1:2 w l title "KEN004", 'KEN002_speglun_good.xy' u 1:2 w l title 'KEN002'
