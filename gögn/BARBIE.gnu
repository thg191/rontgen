set encoding utf8
set term post eps enhanced color solid "Helvetica" 20
set xlabel "m²"
set ylabel "{{/Symbol q}_m}^2 [°]"
set mxtics
set title "" 
#set yrange [0:30]
#set xrange [0:30]
#set logscale y
set output ARG1
#'theta_m.eps'
f(x) = a*x + b
fit f(x) ARG2 u ($2**2):($1**2) via a,b
plot ARG2 using ($2**2):($1**2):(2*$1*$3) with yerrorbars notitle, f(x) title\
    sprintf("%.5fm² + %.3f", a, b)

