#!/usr/env/bin python

import sys
import numpy as np

def get_wasted(theta, beta):
    lambo = 1.5405940e-10
    k = 0.9
    theta = np.cos(np.radians(theta/2))
    return k*lambo/(np.radians(beta)*theta)

theta = float(sys.argv[1])
beta = float(sys.argv[2])

print(get_wasted(theta, beta))
