#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import math

#Opna gefna skrá
f = open(str(sys.argv[1]), 'r')
#Opna skrá til að skrá gögn í
fw = open(str(sys.argv[2]), 'w')
#Neðri og efri mörk
lo = float(sys.argv[3]) 
hi = float(sys.argv[4])
m = 1

#Þurfum þrjá punkta til að ákvarða hvort punktur sé hápunktur
intensifies = []
kevinbacon = []

#Listi af staðsetningu toppa
tops = []

for line in f:
    line = line.rstrip('\n')
    gildi = line.split(' ')
    if float(gildi[0]) < lo:
        continue
    if float(gildi[0]) > hi:
        break
    i = 0
    kevinbacon[i] = kevinbacon.append(float(gildi[0]))
    intensifies[i] = intensifies.append(float(gildi[1]))
    i = i + 1

for i in range(2, len(kevinbacon) - 1):
    if intensifies[i - 1] < intensifies[i] \
    and intensifies[i+1] < intensifies[i]:
        fw.write(str(kevinbacon[i]/2) + ' ' + str(m) + ' 0.01\n')
        m = m + 1

fw.close()
f.close()

#print(kevinbacon)
#print(intensifies)
#Ítrum í gegnum línur þar til við komum að neðri mörkum
#while last < lo:
#    line = f.readline()
#    line = line.rstrip('\n')
#    gildi = line.split(' ')
#    last = float(gildi[0])
#
#lastdeg = last
#last = float(gildi[1])
#
##Fyllum í curr og nex
#line = f.readline()
#line = line.rstrip('\r\n')
#gildi = line.split(' ')
#curr = float(gildi[1])
#currdeg = float(gildi[0])
#line = f.readline()
#line = line.rstrip('\r\n')
#gildi = line.split(' ')
#nex = float(gildi[1])
#nexdeg = float(gildi[0])
#
##Telja toppa
#m = 0
#delta = 0.01
#
##Athugum hvort curr sé hápunktur
#for line in f:
#    if currdeg > lo and currdeg < hi:
#        if curr > last and curr > nex:
#            m += 1
#            #Deilum currdeg í tvennt því gráðurnar eru 2theta
#            theta = math.pow(currdeg/2, 2)
#            #Skrifum í skrá með 0.02 gráða óvissu
#            fw.write(str(theta) + ' ' + str(m) + ' ' + str(delta) +  '\n')
#        line = line.rstrip('\r\n')
#        gildi = line.split(' ')
#        last = curr
#        curr = nex
#        nex = float(gildi[1])
#        lastdeg = currdeg
#        currdeg = nexdeg
#        nexdeg = float(gildi[0])

#Lokum notuðum skrám
