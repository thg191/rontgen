import numpy as np
import matplotlib
import matplotlib.pyplot as plot

f = open('KEN002_speglun_good.xy')
lines = f.readlines()
eindir = np.empty([499], dtype = np.double)
degs = np.empty([499], dtype = np.double)
j = 0
for line in lines:
    line.rstrip('\n')
    line = [np.double(i) for i in line.split(' ')]
    eindir[j] = line[1]
    degs[j] = line[0]
    j = j + 1

fig, ax = plot.subplots()
ax.semilogy(degs, eindir)
fig.show()
input()
